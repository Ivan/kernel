/// Debugging support
pub mod debug;

/// Devices
pub mod device;

/// Inter-processor interrupts
pub mod ipi;

/// Page table isolation
pub mod pti;

/// Stop function
pub mod stop;

pub mod time;
